# Properties for build flash info script
ADDITIONAL_BUILD_PROPERTIES += \
    ro.ros.version=$(ROS_VERSION) \
    ro.ros.releasetype=$(ROS_BUILDTYPE) \
    ro.ros.build.version=$(PRODUCT_VERSION_MAJOR).$(PRODUCT_VERSION_MINOR) \
    ro.modversion=$(ROS_VERSION) \
    ros.build.type=$(ROS_BUILDTYPE) \
    ros.ota.version= $(shell date +%Y%m%d) \
    ro.ros.tag=$(shell grep "refs/tags" .repo/manifest.xml  | cut -d'"' -f2 | cut -d'/' -f3)

# RROS Platform Display Version
ADDITIONAL_BUILD_PROPERTIES +=
    ro.ros.display.version=$(ROS_DISPLAY_VERSION)
