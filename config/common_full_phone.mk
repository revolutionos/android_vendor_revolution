# Inherit full common Lineage stuff
$(call inherit-product, vendor/ros/config/common_full.mk)

# Required packages
PRODUCT_PACKAGES += \
    LatinIME

# Include Lineage LatinIME dictionaries
PRODUCT_PACKAGE_OVERLAYS += vendor/ros/overlay/dictionaries

$(call inherit-product, vendor/ros/config/telephony.mk)
